#!/bin/bash

SRC_DB=$1
SRC_SERVER=$2
SRC_MASTERPASS=$3

curl -X POST \
    -F master_pwd=$SRC_MASTERPASS \
    -F name=$SRC_DB \
    -F backup_format=zip \
    -o $SRC_DB.zip \
    $SRC_SERVER/web/database/backup
