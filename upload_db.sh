#!/bin/bash

SRC_DB=$1
DEST_DB=$2
DEST_SERVER=$3
DEST_MASTERPASS=$4

curl -X POST \
    -F backup_file=@$SRC_DB \
    -F name=$DEST_DB \
    -F master_pwd=$DEST_MASTERPASS \
    $DEST_SERVER/web/database/restore
