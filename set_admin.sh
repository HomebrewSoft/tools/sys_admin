#!/bin/bash
INSTANCE=$1
DB=$2

USER_ID=2
LOGIN='admin'
USER='odoo'
PASSWORD='$pbkdf2-sha512$25000$JqT0vlcqhZBy7r0XImSMUQ$5V2iXQ1WL6cgAZEWrUaQ31LuABUTTBWh4dNO2hWVkMI2/qVrKoTqFKHev7yRhusfK/HSMC3WMCRVp7eQKAfJHA' # Hardocded has for `admin`

query="UPDATE res_users SET
    login = '$LOGIN',
    password = '$PASSWORD',
    active = 't'
    WHERE id = $USER_ID"
docker exec $INSTANCE\_db psql -U $USER -d $DB -c "$query"
